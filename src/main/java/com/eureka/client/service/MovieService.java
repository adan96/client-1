package com.eureka.client.service;

import com.eureka.client.model.entity.Movie;
import com.eureka.client.model.entity.Response;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class MovieService {

    public ResponseEntity getMovies(){
        try{
            List<Movie> movies = new ArrayList<>();
            movies.add(new Movie(1, "movie-1", "summary-1"));
            movies.add(new Movie(2, "movie-2", "summary-2"));
            movies.add(new Movie(3, "movie-3", "summary-3"));
            return new ResponseEntity(movies, HttpStatus.OK);
        }catch(Exception e){
            return new ResponseEntity<>(
                    new Response(
                            HttpStatus.BAD_REQUEST ,"Ocurrió un error"), HttpStatus.BAD_REQUEST);
        }
    }

}
